/*
 please take note: there are source variable meant to make this gulpfile reuseable. pay attention to those variables, watch task, default task etc. Sass folder are omitted from production. However, every other thing as it is, is found within your repo.

 also check for the proper directory. either specified directly or through a variable.

remember virtually all aspect can be optimized e.g css, images etc. Optimizing them is what puts them in the production folder. thus-

 NODE_ENV=prod gulp



 */


var gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    browserSync = require('browser-sync').create(),
    gulpif = require('gulp-if'),
    uglify = require('gulp-uglify'),
    minifyHTML = require('gulp-minify-html'),
    imagemin = require('gulp-imagemin'),
    pngcrush = require('imagemin-pngcrush'),
    concat = require('gulp-concat');

var env,
    jsSources,
    sassSources,
    htmlSources,
    outputDir,
    sassStyle;

env = process.env.NODE_ENV || 'dev';

if (env==='prod') {
  outputDir = 'prod';
    sassStyle = 'compressed';
}else {
    outputDir = 'dev';
}


sassSources = ['dev/scss/**/*.scss'];
htmlSources = [outputDir + '*.html'];
jsSources = ['dev/js/**/*.js'];



gulp.task('browser-sync', ['sass'], function() {
        browserSync.init({
            proxy: "http://localhost/gulp/dev/"
        });
});

gulp.task('js', function() {
  gulp.src(jsSources)
    .pipe(concat('script.js'))
    .pipe(gulpif(env === 'prod', uglify()))
    .pipe(gulp.dest(outputDir + '/js'))
});

gulp.task('sass', function () {
 return gulp.src(sassSources)
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(sass({
            outputStyle: sassStyle
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(outputDir + '/css'))
        .pipe(browserSync.reload({stream: true}));
});

gulp.task('watch', ['browser-sync'], function(){
  gulp.watch(jsSources, ['js']).on('change', browserSync.reload);
  gulp.watch('dev/scss/*.scss', ['sass']).on('change', browserSync.reload);
  gulp.watch('dev/*.html', ['html']).on('change', browserSync.reload);
  gulp.watch('dev/images/**/*.*', ['images']).on('change', browserSync.reload);
});


gulp.task('html', function() {
  gulp.src('dev/*.html')
    .pipe(gulpif(env === 'prod', minifyHTML()))
    .pipe(gulpif(env === 'prod', gulp.dest(outputDir)))
});

gulp.task('images', function() {
  gulp.src('dev/images/**/*.*')
    .pipe(gulpif(env === 'prod', imagemin({
      progressive: true,
      svgoPlugins: [{ removeViewBox: false }],
      use: [pngcrush()]
    })))
    .pipe(gulpif(env === 'prod', gulp.dest(outputDir + '/images')))
});

gulp.task('pages', function () {
    return gulp.src('dev/*.php')
        .pipe(gulp.dest('prod'))
});

gulp.task('includes', function () {
    return gulp.src('dev/includes/**/*')
    .pipe(gulp.dest('prod/includes'))
});


gulp.task('default', ['html', 'js', 'sass','pages', 'images', 'watch' , 'includes']);